import React from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import { Route, Link, withRouter } from 'react-router-dom';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import withRoot from './withRoot';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@material-ui/core/colors';
import logo from './Images/logo.svg';
import { appName, appAcronym } from './System/systemVariables'
import axios from 'axios';
import { authProviderGraph } from "./Security/authProviderGraph";
let styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: "100%"
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      backgroundColor: grey[900]
    },
    siteTitle: {
      color: "white",
      fontWeight: 800,
      fontSize: 28,
      paddingLeft: 16
    },
    logoContainer: {
      cursor: 'pointer',
      [theme.breakpoints.down(720)]: {
        display: 'none'
      }
    },
    logoImage: {
      height: 32,
    },
    content: {
      flexGrow: 1,
      height: "100%",
      overflow: "auto"
    },
  });
class AppMSAL extends React.Component<RouteComponentProps & WithStyles<typeof styles> & {
  accountInfo: any, logout: any
}, {
  userFullName: string;
  userImage: string;
}> {
  constructor(props: any) {
    super(props);
    this.state = {
      userFullName: "",
      userImage:""
    };
  }
  public axiosConfigImage = (token: string) => {
    return {
      responseType: "arraybuffer" as "text" | "arraybuffer" | "blob" | "document" | "json" | "stream" | undefined,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Authorization": `Bearer ${token}`,
      }
    }
  }
  async componentDidMount() {
    const { accountInfo } = this.props;
    this.setState({ userFullName: accountInfo.account.name })    
    let accessTokenResponseGraph = await authProviderGraph.getAccessToken();
    let res = await axios.get(`https://graph.microsoft.com/v1.0/users/${accountInfo.account.userName}/photo/$value`, this.axiosConfigImage(accessTokenResponseGraph.accessToken));
    let buff = Buffer.from(res.data, 'binary').toString('base64');
    let image = buff ? ("data:image/jpeg;base64," + buff) : "";
    this.setState({ userImage: image })
  }
  render() {
    const { classes, logout } = this.props;
    let { userFullName, userImage } = this.state;
    return (
      <div className={classes.root}>
        <AppBar elevation={2} position="fixed" className={classes.appBar}>
          <Toolbar>
            <Link to="/">
              <div className={classes.logoContainer}>
                <img src={logo} alt="logo" className={classes.logoImage} />
              </div>
            </Link>
            <Link to="/" style={{ textDecoration: 'none' }}>
              <Typography variant="h2" noWrap className={classes.siteTitle}>
                {appName} (MSAL)
              </Typography>
            </Link>
            <div style={{ flexGrow: 1 }}></div>
            <Typography variant="subtitle1" noWrap >
              Hello, {userFullName}
            </Typography>
            <Button color={"secondary"} onClick={logout}>Log Out</Button>
            <IconButton color="primary" size="small">
              <Avatar imgProps={{ style: { borderRadius: 25 } }} style={{ padding: 4, backgroundColor: amber[500] }} src={userImage} ></Avatar>
            </IconButton>
          </Toolbar>
        </AppBar>
        <main style={{ backgroundColor: grey["A400"] }} className={classes.content}>
          <Toolbar />
        </main>
      </div>
    );
  }
}
export default withRouter(withRoot(withStyles(styles)(AppMSAL)));
