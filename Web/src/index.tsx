import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import AppADAL from './AppADAL';
import AppMSALContainer from './AppMSALContainer';
import Error from './System/Error';
import Logout from './System/Logout';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { authContext, withAdalLoginApi } from './Security/authProviderAdal';
import { runWithAdal } from 'react-adal';
import './System/styleOverrides.css';
import TagManager from 'react-gtm-module';
const tagManagerArgs = { gtmId: "GTM-PD5HQ5G" }
const DO_NOT_LOGIN = true;

const onError = (error: any) => {  
  if (error.msg === "login required") {
    authContext.login();
    return (null);
  }
  try {
    if (error && error.message && error.message.indexOf("A silent sign-in request was sent but no user is signed in.") >= 0) {
      localStorage.clear();
      authContext.login();
      return (null)
    }
  }  
  catch(e){}
  return (<Error signIn={()=>{authContext.login()}} logOut={()=>{authContext.logOut()}} error={error}/>)  
}

const AppADALLogin = withAdalLoginApi(AppADAL, () => null, onError);
TagManager.initialize(tagManagerArgs)
runWithAdal(authContext, () => {
ReactDOM.render(<Router><Switch><Route path="/logout" component={ Logout } /><Route render={ () => <AppADALLogin /> } /></Switch></Router>, document.getElementById('root'));
},DO_NOT_LOGIN);

// For MSAL comment out above and uncomment below line
//ReactDOM.render(<Router><Switch><Route path="/logout" component={ Logout } /><Route render={ () => <AppMSALContainer /> } /></Switch></Router>, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
