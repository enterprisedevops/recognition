import React from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import { Route, Link, withRouter } from 'react-router-dom';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import withRoot from './withRoot';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import { grey, amber } from '@material-ui/core/colors';

import { appName, appAcronym, showCards } from './System/systemVariables'
import { authContext, adalApiFetch } from './Security/authProviderAdal';
import { Services } from './Services';
import logo from './Images/logo.svg';
import image1 from './Images/1.jpg';
import image2 from './Images/2.jpg';
import image3 from './Images/3.jpg';
import image4 from './Images/4.jpg';
import header from './Images/header.jpg';
import logo2 from './Images/logo.jpg';


import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import RichTextEditor from 'react-rte';
import axios from 'axios';
//@ts-ignore 
import MuiDownshift from 'mui-downshift';
let styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: "100%"
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      backgroundColor: grey[900]
    },
    siteTitle: {
      color: "white",
      fontWeight: 800,
      fontSize: 28,
      paddingLeft: 16
    },
    logoContainer: {
      cursor: 'pointer',
      [theme.breakpoints.down(720)]: {
        display: 'none'
      }
    },
    logoImage: {
      height: 32,
    },
    user: {
      [theme.breakpoints.down(720)]: {
        display: 'none'
      }
    },
    content: {
      flexGrow: 1,
      height: "100%",
      overflow: "auto"
    },
    container: {
      marginTop: theme.spacing(2),
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    carouselWrapper: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    },
    carousel: {
      [theme.breakpoints.down(586)]: {
        maxWidth: 480
      }
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1)
    },
    opacity: {
      '&::placeholder': {
        opacity: 1,
      }
    },
    rte: {
      '& > div > div > div': {
        minHeight: 104
      }
    },
    preview: {
      width: showCards ? 504 : 616,
      marginLeft: "auto",
      marginRight: "auto",
      border: "2px solid #5a5b5d"
    },
    previewRow: {
      paddingLeft: 9
    },
    wrapper: {
      position: 'relative',
      width: "100%"
    },
    buttonProgress: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      marginTop: -8,
      marginLeft: -16,
    },
    error: {
      paddingTop: theme.spacing(1)
    }
  });
class Main extends React.Component<RouteComponentProps & WithStyles<typeof styles> & {

}, {
  userFullName: string;
  userImage: string;
  activeStep: number;
  images: any[];
  selectedIndex: number;
  filteredItems: any[];
  richTextValue: any;
  sendInProgress: boolean;
  email: string;
  to: string;
  body: string;
  from: string;
  error: string
}> {
  constructor(props: any) {
    super(props);
    this.state = {
      userFullName: "",
      userImage: "",
      selectedIndex: showCards ? 0 : -1,
      images: [image1, image2, image3, image4],
      activeStep: 0,
      filteredItems: [],
      richTextValue: RichTextEditor.createEmptyValue(),
      sendInProgress: false,
      email: "",
      to: "",
      body: "",
      from: "",
      error: ""
    };
  }

  componentDidMount() {
    authContext.getUser(async (errorDesc: string | null, user: any | null) => {
      if (user) {
        let name = `${user.profile.given_name} ${user.profile.family_name}`
        this.setState({ userFullName: name, from: name });
        let res = await adalApiFetch(axios.get, `https://graph.microsoft.com/v1.0/users/${user.profile.unique_name}/photo/$value`, {
          responseType: 'arraybuffer'
        }).then((response: any) => Buffer.from(response.data, 'binary').toString('base64'));
        let image = res ? ("data:image/jpeg;base64," + res) : "";
        this.setState({ userImage: image })
      }
    });
  }

  getSteps() {
    return showCards ? ['Pick A Card', 'Enter Details', 'Preview & Send'] : ['Enter Details', 'Preview & Send'];
  }

  getNextButton(step: number) {
    switch (showCards ? step : (step + 1)) {
      case 1:
        return 'Preview';
      case 2:
        return 'Send';
      default:
        return 'Next';
    }
  }

  getStepContent(step: number) {
    const { classes } = this.props;
    let { images, selectedIndex, to, richTextValue, from } = this.state;
    switch (showCards ? step : (step + 1)) {
      case 0:
        return showCards ? <Grid className={classes.carouselWrapper}><Carousel
          className={classes.carousel}
          autoFocus
          infiniteLoop
          useKeyboardArrows
          selectedItem={selectedIndex}
          thumbWidth={600 / (images.length + 1)}
          onClickItem={(index, item) => { this.setState({ selectedIndex: index }); }}
          onChange={(index, item) => { this.setState({ selectedIndex: index, }); }}
        >
          {
            images.map((url, index) => (
              <div key={index}>
                <img src={url} />
              </div>
            ))
          }
        </Carousel>
        </Grid> : null
      case 1:
        return <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography>To:</Typography>
            <MuiDownshift
              variant="outlined"
              getInputProps={() => { return { startAdornment: <span />, endAdornment: <div />, classes: to ? { input: classes.opacity } : {}, placeholder: to ? to : 'Search User', autoFocus: true } }}
              onStateChange={(changes: any, e: any) => { this.handleStateChange(changes, e) }}
              items={this.state.filteredItems}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography>Because:</Typography>
            <RichTextEditor
              editorClassName={classes.rte}
              toolbarConfig={{
                display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'HISTORY_BUTTONS'],
                INLINE_STYLE_BUTTONS: [
                  { label: 'Bold', style: 'BOLD' },
                  { label: 'Italic', style: 'ITALIC' },
                  { label: 'Underline', style: 'UNDERLINE' }
                ],
                BLOCK_TYPE_BUTTONS: [
                  { label: 'UL', style: 'unordered-list-item' },
                  { label: 'OL', style: 'ordered-list-item' }
                ],
                BLOCK_TYPE_DROPDOWN: []
              }}
              rootStyle={{ fontFamily: "Roboto" }}
              placeholder={`${showCards ? "(Optional) " : ""}Type reason here...`}
              value={richTextValue}
              onChange={(value) => { this.setState({ richTextValue: value, body: richTextValue.toString('html') }); }}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography>From:</Typography>
            <TextField
              fullWidth
              variant="outlined"
              value={from}
              InputProps={{
                readOnly: true
              }}
            />
          </Grid>
        </Grid>
      case 2:
        return <Grid container className={classes.preview}>
          <Grid item>
            <img src={showCards ? images[selectedIndex] : header}></img>
          </Grid>
          <Grid item xs={12} className={classes.previewRow} style={{ marginTop: showCards ? 8 : -8 }}>
            <b>To: </b>{to}
          </Grid>
          <Grid item xs={12}>
            <RichTextEditor readOnly rootStyle={{ fontFamily: "Roboto", border: 'none' }} value={richTextValue} editorStyle={{ fontSize: '0.875rem', marginTop: 7, marginBottom: 7 }} />
          </Grid>
          <Grid item xs={12} className={classes.previewRow}>
            <Grid container>
              <div style={{ marginTop: 16, flexGrow: 1 }}><b>From: </b>{from}</div>
              <img src={logo2} />
            </Grid>
          </Grid>
        </Grid>
      default:
        return null;
    }
  }

  getNameFromEmail = (email?: string): string => {
    if (email && email.indexOf(".") !== -1) {
      let name = email.toLocaleLowerCase();
      return name.split('.')[0].charAt(0).toUpperCase() + ' ' + name.split('.')[1].split('@')[0].charAt(0).toUpperCase();
    }
    else return "N/A";
  }

  handleStateChange = async (changes: any, e: any) => {
    if (changes.selectedItem) {
      this.setState({ email: changes.selectedItem.email, to: changes.selectedItem.fullName });
      this.setState({ filteredItems: [] });
      e.clearSelection();
    }
    else if (typeof changes.inputValue === 'string' && changes.inputValue !== "") {
      let results = await Services.GetUserPeople(changes.inputValue.toLowerCase());
      this.setState({ filteredItems: results });
    }
    else if (changes.inputValue === "") {
      this.setState({ filteredItems: [] });
    }
  }

  missingFields = (): boolean => {
    const { activeStep, to, from, richTextValue } = this.state;
    switch (showCards ? activeStep : (activeStep + 1)) {
      case 1:
        if (showCards) return to.length === 0 || from.length === 0;
        return to.length === 0 || from.length === 0 || richTextValue.getEditorState().getCurrentContent().getPlainText().trim().length === 0;
      default:
        return false;
    }
  }

  handleReset = () => {
    this.setState({ activeStep: 0, filteredItems: [], richTextValue: RichTextEditor.createEmptyValue(), email: "", to: "", body: "" });
  }

  handleBack = () => {
    let { activeStep } = this.state;
    this.setState({ activeStep: activeStep - 1, error: "" });
  }

  handleNext = () => {
    let { activeStep } = this.state;
    this.setState({ activeStep: activeStep + 1 });
  }

  send = () => {
    let { activeStep, selectedIndex, email, to, body, from } = this.state;
    this.setState({ sendInProgress: true, error: "" });
    Services.SendEmail(selectedIndex, email, to, body, from).then(() => {
      this.setState({ sendInProgress: false, activeStep: activeStep + 1 });
    }, (err) => {
      let error = "There was an error sending card. Please try again.";
      if (err.response && err.response.status === 429 && err.response.data) {
        if (err.response.data.indexOf('1m') !== -1) {
          error = "You've sent too many cards in the past minute. Please wait and try again.";
        }
        else if (err.response.data.indexOf('1h') !== -1) {
          error = "You've sent too many cards in the past hour. Please wait and try again.";
        }
      }
      this.setState({ sendInProgress: false, error: error });
    })
  }

  render() {
    const { classes } = this.props;
    let { userFullName, userImage, activeStep, email, sendInProgress, error } = this.state;
    const steps = this.getSteps();
    return (
      <div className={classes.root}>
        <AppBar elevation={2} position="fixed" className={classes.appBar}>
          <Toolbar>
            <Link to="/">
              <div className={classes.logoContainer}>
                <img src={logo} alt="logo" className={classes.logoImage} />
              </div>
            </Link>
            <Link to="/" style={{ textDecoration: 'none' }}>
              <Typography variant="h2" noWrap className={classes.siteTitle}>
                {appName}
              </Typography>
            </Link>
            <div style={{ flexGrow: 1 }}></div>
            <Typography color="primary" variant="subtitle1" noWrap className={classes.user}>
              {userFullName}
            </Typography>
            <Button color={"secondary"} onClick={() => authContext.logOut()}>Log Out</Button>
            <IconButton color="primary" size="small">
              <Avatar imgProps={{ style: { borderRadius: 25 } }} style={{ padding: 4, backgroundColor: amber[500] }} src={userImage} ></Avatar>
            </IconButton>
          </Toolbar>
        </AppBar>
        <main className={classes.content}>
          <Toolbar />
          <Container className={classes.container} maxWidth={"md"}>
            <Stepper activeStep={activeStep} orientation="vertical">
              {steps.map((label, index) => {
                return (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                    <StepContent>
                      {this.getStepContent(activeStep === index ? activeStep : -1)}
                      <Grid container>
                        <Button variant="contained" color="secondary" disabled={sendInProgress || activeStep === 0} onClick={this.handleBack} className={classes.button}>
                          Back
                        </Button>
                        <div>
                          <span className={classes.wrapper}>
                            <Button variant="contained" color="primary" onClick={activeStep === steps.length - 1 ? this.send : this.handleNext} disabled={sendInProgress || this.missingFields()} className={classes.button}>
                              {this.getNextButton(activeStep)}
                            </Button>
                            {sendInProgress && <CircularProgress size={24} className={classes.buttonProgress} />}
                          </span>
                        </div>
                      </Grid>
                      <Grid container className={error ? classes.error : ""}>
                        <Typography color="error">{error}</Typography>
                      </Grid>
                    </StepContent>
                  </Step>
                );
              })}
              {activeStep === steps.length &&
                <Grid container>
                  <Grid container className={classes.instructions}>
                    Your card has been sent to {email}.
                  </Grid>
                  <Grid container>
                    <Button variant="contained" color="primary" onClick={this.handleReset}>
                      Send Another Card
                    </Button>
                  </Grid>
                </Grid>}
            </Stepper>
          </Container>
        </main>
      </div>
    );
  }
}
export default withRouter(withRoot(withStyles(styles)(Main)));
