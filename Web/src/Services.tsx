
import axios from 'axios';
import { adalApiFetch, getToken } from './Security/authProviderAdal';
var _ = require('lodash');

export class Services {
    public static GetPhoto = async (email: string) => {
        let res = await adalApiFetch(axios.get, `https://graph.microsoft.com/v1.0/users/${email}/photo/$value`, {
            responseType: 'arraybuffer'
        }).then((response: any) => Buffer.from(response.data, 'binary').toString('base64'))
            .catch((error: any) => { });
        return res ? ("data:image/jpeg;base64," + res) : undefined;
    }

    public static axiosConfig = (token: string) => {
        return {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Authorization": `Bearer ${token}`,
            }
        }
    }

    public static GetUserPeople = async (term: string) => {
        let people: any = [];
        await adalApiFetch(axios.get, `https://graph.microsoft.com/v1.0/me/people/?$search=${term}&$filter=personType/class eq 'Person' and personType/subclass eq 'OrganizationUser'`, {}).then((response: any) => {
            people = response.data.value;
        });
        await adalApiFetch(axios.get, `https://graph.microsoft.com/v1.0/users/?$top=50&$filter=startswith(givenName,'${term}') or startswith(surname,'${term}')`, {}).then((response: any) => {
            people = [...people, ...response.data.value]
        });
        people = _.uniqBy(people, "userPrincipalName");
        return people
            .filter((person: any) => { return person.userPrincipalName != null && person.givenName != null && person.displayName.indexOf("Admin") === -1 })
            .map((item: any) => {
                let name = `${item.givenName} ${item.surname}`;
                return { label: `${name} - ${item.userPrincipalName}`, email: item.userPrincipalName, fullName: `${name}` }
            });
    }

    public static SendEmail = async (selectedIndex: number, email: string, to: string, body: string, from: string): Promise<number> => {
        let token: string = await getToken();
        let res = await axios.post("/api/Email", { "Image": selectedIndex, "Email": email, "To": to, "Body": body, "From": from }, Services.axiosConfig(token));
        return res.data;
    }
}
export default Services;