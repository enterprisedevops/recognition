import React from "react";
import { AzureAD,  AuthenticationState} from "react-aad-msal";
import { authProviderMsal } from "./Security/authProviderMsal";
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import amber from '@material-ui/core/colors/amber';
import AppMSAL from './AppMSAL';
class AppMSALContainer extends React.Component<{}, {}> {
  render() {
    return (
        <AzureAD provider={authProviderMsal} forceLogin={true}>
          {({ login, logout, authenticationState, error, accountInfo }: { login: any, logout: any, authenticationState: any, error: any, accountInfo: any }) => {
            
            if (error) console.log(error)
            if (authenticationState === AuthenticationState.Unauthenticated && error && error.errorCode === "invalid_state_error"){
              console.log("invalid state error found")
              login();
            }
            // User is authenticated, app can load
            if (authenticationState === AuthenticationState.Authenticated) {
              return (
                <AppMSAL logout={logout} accountInfo={accountInfo}/>
              );
            }
            else {
              return (
                <Backdrop style={{ color: amber[500], zIndex: 3000,backgroundColor:"rgba(0, 0, 0, 0.8)" }} open={true} transitionDuration={0}>
                  <CircularProgress color="inherit" />
                </Backdrop>
              );
            }
          }
          }
        </AzureAD>
    );
  }
}

export default AppMSALContainer;
