import { AdalConfig, AuthenticationContext, adalFetch, withAdalLogin} from 'react-adal';
import {clientId, endpoint, tenant} from './authProviderVariables';
export const adalConfig: AdalConfig = {
  cacheLocation: 'localStorage',
  clientId: clientId,
  endpoints: {
      api:endpoint
  },
  redirectUri:window.location.origin + '/',
  postLogoutRedirectUri: window.location.origin + '/logout',
  tenant: tenant
};
export const authContext = new AuthenticationContext(adalConfig);
export const adalApiFetch = (fetch:any, url:any, options:any) => adalFetch(authContext, 'https://graph.microsoft.com' , fetch, url, options);
export const withAdalLoginApi = withAdalLogin(authContext, adalConfig.endpoints ? adalConfig.endpoints.api : '');
export const getToken = (): string | Promise<string> => {
  let token = authContext.getCachedToken(authContext.config.clientId);
  if (!token) {
    return new Promise((resolve, reject) => {
      authContext.acquireToken(authContext.config.clientId, (message:any, token:any) => {
        if (!token) {
          const err = new Error(message?message:"No Message");
          reject(err);
        } else {
          resolve(token);
        }
      });
    });
  }
  return token;
};
