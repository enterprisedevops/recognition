export const endpoint = 'https://hdsupplyinc.sharepoint.com'; // Endpoint URL
export const clientId = 'a325c591-07ff-49bf-a53f-da30bd4b518a'; // App Registration ID
export const tenant = 'hdsupplyinc.onmicrosoft.com';
export const tenantId = 'fe99bbb3-bdc3-4260-998e-c28159c895a2';
export const graphScopes = ["https://graph.microsoft.com/.default"]
export const msalScopes = [`api://${clientId}/access`] // Revert back to clientId after testing
