import * as React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blueGrey from '@material-ui/core/colors/blueGrey';
import amber from '@material-ui/core/colors/amber';
import CssBaseline from '@material-ui/core/CssBaseline';
import createBreakpoints from '@material-ui/core/styles/createBreakpoints';
const breakpoints = createBreakpoints({});
const theme = createMuiTheme({
  breakpoints,
  palette: {
    primary: amber,
    secondary: blueGrey,
  },
  overrides: {
    MuiTypography: {
      h1: {
        [breakpoints.down('sm')]: {
          fontSize: pxToRem(48)
        }
      },
      h2: {
        [breakpoints.down('sm')]: {
          fontSize: pxToRem(30)
        }
      },
      h3: {
        [breakpoints.down('sm')]: {
          fontSize: pxToRem(24)
        }
      }
    }
  }
});
function withRoot<P>(Component: React.ComponentType<P>) {
  function WithRoot(props: P) {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    );
  }
  return WithRoot;
}
function pxToRem(value: number) {
  return `${value / 16}rem`;
}
export default withRoot;
