/* Main Imports */
import React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import withRoot from '../withRoot';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
/* Declaration ends */

/* Material Core Imports */
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { grey, blueGrey, amber } from '@material-ui/core/colors';
import logo from '../Images/logo.svg';
import {appName, appAcronym} from './systemVariables'

const styles = (theme: Theme) =>
  createStyles({
    appBar: {
      position: 'relative',
      backgroundColor: grey[900]
    },
    logoContainer: {
      background: grey[900],
      padding: theme.spacing(1),
      margin: theme.spacing(1, 1, 0, 0),
      borderRadius: 20,
      [theme.breakpoints.down(720)]: {
        display: 'none'
      }
    },
    logoImage: {
      height: 16,
      width: 120
    },
    logoAvatar: {
      boxShadow: "rgba(0, 0, 0, 0.2) 0px 1px 8px 0px, rgba(0, 0, 0, 0.14) 0px 3px 4px 0px, rgba(0, 0, 0, 0.12) 0px 3px 3px -2px",
      background: grey[800],
      width: 48,
      height: 48,
      fontSize: '1rem',
      fontWeight: 500
    },
    grow: {
      flexGrow: 1
    },
    heroUnitMain: {
      backgroundImage: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAWCAYAAADafVyIAAAAV0lEQVR4AWPg5uaexcDI8B9Ec3JyFhDCpKpnACmGYWI0kKqe9j4AAi0gBtPEaCBZPYggF1NsAYXBR9gCShPAwPuAkgRA30imJR4tKkaLitGiYrSoIAIDAKy7LKCTTHSAAAAAAElFTkSuQmCC')",
      backgroundColor: grey[800]
    },
    heroContent: {
      margin: '0 auto',
      padding: theme.spacing(2, 0, 1)
    }
  });

class Error extends React.Component<
  {
    error: any;
    signIn: Function;
    logOut: Function;
  } & WithStyles<typeof styles>
  > {
  constructor(props: any) {
    super(props);
  }
  componentDidMount() {
    localStorage.clear();
  }
  render() {
    const { classes, signIn, logOut, error } = this.props;
    return (
      <React.Fragment>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <div className={classes.logoContainer}>
              <img src={logo} alt="logo" className={classes.logoImage} />
            </div>
            <Typography variant="h2" noWrap style={{ color: grey[200], fontWeight: 800 }}>
              <Avatar className={classes.logoAvatar} title={appName}>{appAcronym}</Avatar>
            </Typography>
            <div className={classes.grow} />
            <Button variant="contained" color="primary" onClick={() => logOut()}>
              Log Out
            </Button>
          </Toolbar>
        </AppBar>
        <Slide direction="down" in={true} mountOnEnter unmountOnExit>
          <div className={classes.heroUnitMain}>
            <div className={classes.heroContent} style={{ color: blueGrey[100] }}>
              <Typography variant="h2" align="center" style={{ color: grey[50], fontWeight: 800 }} gutterBottom>
                {appName}
              </Typography>
            </div>
          </div>
        </Slide>
        <Container maxWidth="md" style={{ paddingTop: 40 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Typography variant="h5" gutterBottom>Sign in</Typography>
              <Typography>
                Sorry, but we're having trouble signing you in. Please sign in again or log out and retry with a different account.
              </Typography>
            </Grid>
            <Grid container spacing={2} justify="center" style={{ padding: 20 }}>
              <Grid item>
                <Button variant="contained" color="secondary" onClick={() => signIn()}>Sign In Again</Button>
              </Grid>
              <Grid item>
                <Button variant="contained" color="primary" onClick={() => logOut()}>Log Out</Button>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Typography>
                If issue persists, please contact the Enterprise DevOps team at <a href="mailto:HDSGSCEnterpriseDevOps@HDSupply.com">HDSGSCEnterpriseDevOps@HDSupply.com</a> to request access.
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Typography variant="caption" style={{ whiteSpace: 'pre-wrap' }}>
                {error.message ? error.message : error}
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </React.Fragment>
    );
  }
}
export default withRoot(withStyles(styles)(Error));
