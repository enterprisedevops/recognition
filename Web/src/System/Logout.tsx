/* Main Imports */
import React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import withRoot from '../withRoot';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

/* Material Core Imports */
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { grey, blueGrey, amber } from '@material-ui/core/colors';
import { RouteComponentProps } from 'react-router-dom';
import {appName, appAcronym} from './systemVariables'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: grey[800],
      height: '100%',
      color: grey[50]
    },
    heroUnitMain: {
      paddingTop: 200
    },
    heroContent: {
      margin: '0 auto',
      padding: theme.spacing(2, 0, 1)
    }
  });
class Logout extends React.Component<RouteComponentProps & WithStyles<typeof styles>> {
  render() {
    const { classes, history } = this.props;
    let msg = "You have successfully signed out";
    if (localStorage.getItem("timeout") !== null) {
      msg = "Your session has timed out due to inactivity";
      localStorage.removeItem("timeout");
    }
    return (
        <div className={classes.root}>
          <Slide direction="down" in={true} mountOnEnter unmountOnExit>
            <div className={classes.heroUnitMain}>
              <div className={classes.heroContent} style={{ color: blueGrey[100] }}>
                <Typography variant="h2" align="center" style={{ fontWeight: 800 }} gutterBottom>
                {appName}
                </Typography>
              </div>
            </div>
          </Slide>
          <Container maxWidth="md">
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography align="center">
                  {msg}
                </Typography>
              </Grid>
              <Grid container spacing={2} justify="center" style={{ padding: 20 }}>
                <Grid item>
                  <Button variant="contained" color="primary" onClick={() => history.push('/')} style={{ width: 200 }}>Sign In</Button>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </div>
    );
  }
}
export default withRoot(withStyles(styles)(Logout));
