import React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import withRoot from '../withRoot';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

const styles = (theme: Theme) => createStyles({});

class IdleModal extends React.Component<
  {
    showModal: boolean;
    handleClose: any;
    handleLogout: any;
  } & WithStyles<typeof styles>,
  {
    timer: number;
  }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      timer: 120
    };
  }

  handleEntered = () => {
    this.setState({
      timer: 120
    });

    let interval = setInterval(() => {
      if (!this.props.showModal) clearInterval(interval);

      this.setState(
        prevState => {
          return { timer: prevState.timer - 1 };
        },
        () => {
          if (this.props.showModal && this.state.timer === 0) this.props.handleLogout();
        }
      );
    }, 1000);
  };

  render() {
    const { showModal, handleClose, handleLogout } = this.props;
    const { timer } = this.state;
    return (
      <Dialog open={showModal} onClose={handleClose} onEntered={this.handleEntered} disableBackdropClick disableEscapeKeyDown>
        <DialogTitle>Your session is about to expire.</DialogTitle>
        <DialogContent>
          <DialogContentText>You will be logged out in {timer} seconds</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleLogout}>Logout Now</Button>
          <Button onClick={handleClose}>Stay signed In</Button>
        </DialogActions>
      </Dialog>
    );
  }
}
export default withRoot(withStyles(styles)(IdleModal));
