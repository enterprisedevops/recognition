﻿using Microsoft.Extensions.FileProviders;
using Recognition.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recognition.Services
{
    public interface IEmailRepository
    {
        bool Post(EmailAction email, string userEmail);
    }
}
