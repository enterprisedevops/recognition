﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Recognition.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Recognition.Services
{
    public class EmailRepository : IEmailRepository
    {
        private readonly IConfiguration _config;
        private readonly ILogger<EmailRepository> _logger;
        public EmailRepository(IConfiguration config, ILogger<EmailRepository> logger)
        {
            _config = config;
            _logger = logger;
        }
        public bool Post(EmailAction email, string userEmail)
        {
            try
            {
                var emailConfig = _config.GetSection("EmailConfig");
                SmtpClient mailClient = new SmtpClient(emailConfig["SmtpClient"]);
                mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailMessage emailMessage = new MailMessage();
                emailMessage.From = new MailAddress(emailConfig["FromAddress"], emailConfig["FromName"]);
                emailMessage.To.Add(new MailAddress(email.Email, email.To));
                emailMessage.Subject = $"You have been recognized by {email.From}!";
                emailMessage.IsBodyHtml = true;
                emailMessage.AlternateViews.Add(GetMailBody(email));
                mailClient.Send(emailMessage);
                _logger.LogInformation($"Success: {userEmail} - {email.Email}");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failure: {userEmail} - {email.Email}: {ex.Message}");
            }
            return false;
        }

        private AlternateView GetMailBody(EmailAction email)
        {
            LinkedResource img = new LinkedResource(File.OpenRead(Path.Combine("Images", email.Image == -1 ? "header.jpg" : $"{email.Image + 1}.jpg")), MediaTypeNames.Image.Jpeg);
            img.ContentId = "Card";
            LinkedResource logo = new LinkedResource(File.OpenRead(Path.Combine("Images", "logo.jpg")), MediaTypeNames.Image.Jpeg);
            logo.ContentId = "Logo";
            string body = $@"
            <table align='center' style='width: 500px; margin-left: auto; margin-right: auto; border: 2px solid #5a5b5d; border-collapse: collapse;'>
                <tbody>
                    <tr><td colspan='2' style='padding: 0;'><img src=cid:Card id='img' /></td></tr>
                    <tr><td colspan='2' style='padding: {(email.Image == -1 ? 0 : 16)}px 8px 0;'><b>To: </b>{email.To}</td></tr>
                    <tr><td colspan='2' style='padding: 16px 8px;'>{email.Body}</td></tr>
                    <tr><td style='padding: 0 8px; vertical-align: top; margin-top: 16px;'><b>From: </b>{email.From}</td><td style='padding: 0; text-align: right;'><img src=cid:Logo id='logo' /></td></tr>
                </tbody>
            </table>";
            AlternateView view = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            view.LinkedResources.Add(img);
            view.LinkedResources.Add(logo);
            return view;
        }
    }
}
