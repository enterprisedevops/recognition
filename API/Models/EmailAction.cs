﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recognition.Models
{
    public class EmailAction
    {
        public int Image { get; set; }
        public string Email { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
    }
}
