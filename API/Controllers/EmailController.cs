﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Recognition.Models;
using Recognition.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Recognition.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailRepository _emailRepository;

        public EmailController(IEmailRepository emailRepository)
        {
            _emailRepository = emailRepository;
        }
        [HttpPost]
        public ActionResult Post([FromBody] EmailAction email)
        {
            string userEmail = User.FindFirst(ClaimTypes.Upn)?.Value;
            bool result = _emailRepository.Post(email, userEmail);
            return result ? Ok() : StatusCode(500);
        }
    }
}
